package com.example.gsonexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void readjson(View view) {

        String json_string  = null;

        try {

            InputStream inputStream = getAssets().open("abc.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json_string = new String(buffer,"UTF-8");
            Toast.makeText(getApplicationContext(),json_string,Toast.LENGTH_LONG).show();
            Gson gson = new Gson();
            post post = gson.fromJson(json_string, com.example.gsonexample.post.class);
            Toast.makeText(getApplicationContext(),post.Personal_details.name,Toast.LENGTH_LONG).show();

        }catch (IOException e)
        {

            e.printStackTrace();
        }


    }
}
